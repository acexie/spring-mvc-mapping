<%@ page pageEncoding="UTF-8"%>
<html>
  <head>
    <title>Result Page</title>
  </head>
  <body>
    <h1>${message}</h1>
    <h3>
      <a href="/spring-mvc-mapping/class-level-mapping">Click here to see more usage</a>
    </h3>
  </body>
</html>