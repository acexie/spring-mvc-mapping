package org.lychie.controller;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 注解 @RequestMapping 标注在方法级别上的示例
 * 
 * @see ClassLevelMappingController
 * 
 * @author Lychie Fan
 * 
 * <http://lychie.github.io 2015-03-21>
 */
@Controller
public class MethodLevelMappingController {

	@RequestMapping(value = "/method-level-mapping", method = GET)
	public String execute(Model model) {
		model.addAttribute("message", "mapped by /method-level-mapping");
		return "method-level-mapping/result";
	}
	
}