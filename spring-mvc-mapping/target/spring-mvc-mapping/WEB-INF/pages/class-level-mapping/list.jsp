<%@ page pageEncoding="UTF-8"%>
<html>
  <head>
    <title>Class Level Mapping</title>
    <script type="text/javascript" src="http://code.hs-cn.com/jquery/jquery-1.7.1.min.js"></script>
  </head>
  <body>
    <h1>Welcome to the class-level-mapping list page!</h1>
    <h3>
      <p><a href="class-level-mapping/pathname">class-level-mapping/pathname</a></p>
      <p><a href="class-level-mapping/pathname/public">class-level-mapping/pathname/public</a></p>
      <p><a href="class-level-mapping/pathname/sample.html">class-level-mapping/pathname/sample.html</a></p>
      <p><a href="class-level-mapping/parameter">class-level-mapping/parameter</a></p>
      <p><a href="class-level-mapping/parameter?p=s">class-level-mapping/parameter?p=s</a></p>
      <p><a href="class-level-mapping/parameter?param">class-level-mapping/parameter?param</a></p>
      <p><a href="class-level-mapping/parameter?param=s">class-level-mapping/parameter?param=s</a></p>
      <p><a href="class-level-mapping/argument">class-level-mapping/argument</a></p>
      <p><a href="class-level-mapping/argument?param=1">class-level-mapping/argument?param=1</a></p>
      <p><a href="class-level-mapping/argument?param=2">class-level-mapping/argument?param=2</a></p>
      <p><a href="class-level-mapping/header">class-level-mapping/header</a></p>
      <p><a href="javascript:;" class="post">class-level-mapping/header [POST]</a></p>
      <p><a href="class-level-mapping/produce">class-level-mapping/produce</a></p>
      <p><a href="javascript:;" class="consume">class-level-mapping/consume [POST]</a></p>
      <p><a href="class-level-mapping/foo/foobar">class-level-mapping/foo/foobar</a></p>
      <p><a href="class-level-mapping/food/fruit/banana">class-level-mapping/food/fruit/banana</a></p>
      <p><a href="class-level-mapping/spring-mvc-mapping.zip">class-level-mapping/spring-mvc-mapping.zip</a></p>
      <p><a href="class-level-mapping/param/require?color=blue">class-level-mapping/param/require?color=blue</a></p>
      <p><a href="class-level-mapping/param/rename?cr=blue">class-level-mapping/param/rename?cr=blue</a></p>
      <p><a href="class-level-mapping/param/negation">class-level-mapping/param/negation</a></p>
      <p><a href="class-level-mapping/param/negation?color=blue">class-level-mapping/param/negation?color=blue</a></p>
    </h3>
    <textarea style="display:none">{"id":1001,"name":"Lychie Fan"}</textarea>
    <script type="text/javascript">
      $(function(){
    	  $(".post").click(function(){
    		  $(this).after("<form method='post'></form>");
    		  var form = $("form");
        	  form.prop("action", "class-level-mapping/header");
        	  form.submit();
    	  });
    	  $(".consume").click(function(){
    		  $.ajax({
    			  type : "post",
    			  url : "class-level-mapping/consume",
    			  data : $("textarea").val(),
    			  contentType : "application/json",
    			  success : function(){
    				  alert("well, now you can see the execution result on the console");
    			  }
    		  });
    	  });
      });
    </script>
  </body>
</html>